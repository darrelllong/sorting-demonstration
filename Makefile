OLD=true
MASK=0x00ffffff
CFLAGS=-Wall -Werror -Wextra -pedantic -O3
CC=gcc
OBJS=sorting.o bubblesort.o minsort.o insertionsort.o quicksort.o \
mergesort.o shellsort.o heapsort.o stack.o

sorting	: Makefile $(OBJS)
	$(CC) -o sorting $(OBJS)

sorting.o	:	sorting.c
	$(CC) $(CFLAGS) -DOLD=$(OLD) -DMASK=$(MASK) -c sorting.c

clean	:
	rm -f $(OBJS) sorting
